﻿using BrowseEmAll.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebScrapingExample
{
    class Program
    {
        static BrowserManager manager = new BrowserManager(IntegrationMode.Standalone);

        static void Main(string[] args)
        {
            manager.OnBrowserReady += Manager_OnBrowserReady;
            manager.OnDocumentComplete += Manager_OnDocumentComplete;
            manager.OnJavaScriptExecuted += Manager_OnJavaScriptExecuted;

            manager.OpenBrowser(Browser.CHROME48, new BrowseEmAll.API.Models.BrowserSettings());

            Console.ReadLine();
        }

        private static void Manager_OnJavaScriptExecuted(object sender, BrowseEmAll.API.Events.JavaScriptExecuteEventArgs e)
        {
            // Write all Question titles to the console
            string result = e.Value.ToString();

            string[] questions = result.Split('|');

            foreach (string question in questions)
            {
                Console.WriteLine(question);
            }

            manager.CloseBrowser(e.BrowserID);
        }

        private static void Manager_OnDocumentComplete(object sender, BrowseEmAll.API.Events.DocumentCompletedEventArgs e)
        {
            // Get all the question titles using JavaScript
            // You could also just return the complete HTML with
            // document.innerHTML and do the processing in c#
            string javascript = @"var questions = document.getElementsByClassName('question-hyperlink');var result = '';for (var i in questions) { result += questions[i].innerHTML + '|'; } return result;";

            manager.ExecuteJavaScript(e.BrowserID, javascript);
        }

        private static void Manager_OnBrowserReady(object sender, BrowseEmAll.API.Events.BrowserReadyEventArgs e)
        {
            // Browser is ready for commands
            Console.WriteLine("Browser Ready!");

            manager.Navigate(e.BrowserID, "http://stackoverflow.com/");
        }
    }
}

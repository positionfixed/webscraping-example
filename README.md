# README #

This example demonstrates how you can do web scraping using the BrowseEmAll Core API in a .Net Application. 

The Core API is necessary to run the example, get it at [BrowseEmAll](https://www.browseemall.com/CoreAPI).